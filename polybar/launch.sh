#!/usr/bin/env sh

#terminate running bars
killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep1; done

#launch bar
polybar example &

echo "Bars launched..."
